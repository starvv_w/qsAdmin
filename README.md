# qsAdmin
基于Layui的后台管理系统前端模板，主要参考nepAdmin前端模板

## 简介
以前做cs产品开发，现在要转型了到bs结构还是有诸多的不适应，要学的东西太多太多，慢慢积累。

## 软件架构
```shell
qsAdmin  主程序目录
├── json  本地json数据
├── static  静态文件
│   ├── images  图片文件
│   ├── font  字体文件
│   ├── css  样式文件
│   ├── js  脚本文件
│   │   └── extends  扩展脚本文件(根据layui规则扩展)
│   └── plugins  第三方组件(基本不需要改动的)
│       ├── font-awesome  图标组件
│       ├── ie8hack  ie支持文件
│       └── layui  基础框架
├── pages  展示页面
└── index.html  入口文件
```

## 组件说明

### Layui
目前基于Layui 2.4.5版本
官网： http://www.layui.com/

### echarts
目前基于echarts 4.0.2版本
官网： http://echarts.baidu.com/ 

## 参考资料
* [贤心 Layui](https://www.layui.com/)
* [june nepadmin](https://gitee.com/june000/nep-admin)
