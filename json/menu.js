{
    "code":0,
    "msg":"ok",
    "data":
    [{
        "title": "首页",
        "icon": "layui-icon-home",
        "href": "/"
    },{
        "title": "首页[0]",
        "icon": "layui-icon-home",
        "href": "/index2"
    }, {
        "title": "用户管理",
        "icon": "layui-icon-unorderedlist",
        "childs": [{
            "title": "用户角色",
            "href": "/list/table/id=1"
        }, {
            "title": "用户列表",
            "href": "/list/card"
        }, {
            "title": "我的用户",
            "href": "/test/test"
        }]
    }, {
        "title": "列表页",
        "icon": "layui-icon-unorderedlist",
        "childs": [{
            "title": "表格列表",
            "href": "/list/table/id=1"
        }, {
            "title": "卡片列表",
            "href": "/list/card"
        }, {
            "title": "我的测试",
            "href": "/test/test"
        }]
    }, {
        "title": "详情页",
        "icon": "layui-icon-container",
        "childs": [{
            "title": "工作计划",
            "href": "/detail/plan"
        }, {
            "title": "数据统计",
            "href": "/chart/index"
        }]
    }, {
        "title": "表单页",
        "icon": "layui-icon-file-exception",
        "childs": [{
            "title": "表单元素",
            "href": "/form/basic"
        }, {
            "title": "表单组合",
            "href": "/form/group"
        }]
    }, {
        "title": "异常页",
        "icon": "layui-icon-error",
        "childs": [{
            "title": "403",
            "href": "/exception/403"
        }, {
            "title": "404",
            "href": "/exception/404"
        }, {
            "title": "500",
            "href": "/exception/500"
        }]
    }, {
        "title": "新增模块",
        "icon": "layui-icon-experiment",
        "notice": 3,
        "childs": [{
            "title": "admin",
            "href": "/module/admin"
        }, {
            "title": "helper",
            "href": "/module/helper"
        }, {
            "title": "loadBar",
            "href": "/module/loadbar"
        }]
    }, {
        "title": "图标",
        "icon": "layui-icon-star",
        "href": "/icon/index"
    }, {
        "title": "多级导航",
        "icon": "layui-icon-apartment",
        "childs": [{
            "title": "二级菜单",
            "childs": [{
                "title": "三级菜单1",
                "childs": [{
                    "title": "四级403",
                    "href": "/exception/403"
                }, {
                    "title": "四级404",
                    "href": "/exception/404"
                }, {
                    "title": "四级500",
                    "href": "/exception/500"
                }, {
                    "title": "法力虚空",
                    "href": "/exception/403"
                }]
            }, {
                "title": "三级菜单2",
                "childs": [{
                    "title": "肉钩",
                    "href": "/exception/403"
                }, {
                    "title": "腐烂",
                    "href": "/exception/403"
                }, {
                    "title": "腐肉堆积",
                    "href": "/exception/403"
                }, {
                    "title": "肢解"
                }]
            }]
        }, {
            "title": "二级没下级",
            "childs": []
        }]
    }]
}